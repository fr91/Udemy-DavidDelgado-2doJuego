﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {
    private float destroyTime = 2.0f;

	// Use this for initialization
	void Start () {
        Invoke("Die", destroyTime);

	}

	// Update is called once per frame
	void Update () {
		
	}

    void Die()
    {
        Destroy(gameObject);
    }

}
