﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceToObject : MonoBehaviour
{

    private Transform objectToFollow = null;
    private Transform theTransform = null;
    public bool followPlayer = false;
    private void Awake()
    {

        //if (!followPlayer)
        //    return;
        theTransform = GetComponent<Transform>();
        GameObject thePlayer = GameObject.FindWithTag("Player");
        if (thePlayer != null)
        {
            objectToFollow = thePlayer.GetComponent<Transform>();
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (objectToFollow == null)
            return;
        Vector3 directionToFollow = objectToFollow.position - theTransform.position;
        theTransform.localRotation = Quaternion.LookRotation(directionToFollow, Vector3.up);
    }
}
