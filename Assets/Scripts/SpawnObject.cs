﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{

    public float maxRadius = 1.0f;
    public float interval = 5.0f;
    public GameObject objectToSpawn = null;
    private Transform origin = null;

    // Awake is called when the script instance is being loaded
    private void Awake()
    {
        origin = GameObject.FindWithTag("Player").GetComponent<Transform>();
    }


    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Spawn", 0.0f, interval);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Spawn()
    {
        Vector3 lookToTheOtherSide = Vector3.zero;
        if (origin == null)
            return;

        //Generamos la posición aleatoria
        float verticalPoint = Random.Range(-9.0f, 9.0f);
        float horizontalPoint = Mathf.Round(Random.Range(-9.0f, 9.0f));
        if (horizontalPoint > -9.0f && horizontalPoint < 9.0f)
        {
            verticalPoint = (Mathf.Round(Random.value) * 2 - 1) * 9.0f;
            Debug.Log("horizontalPoint: " + verticalPoint);
        }
        Vector3 spawnPosition = new Vector3(verticalPoint, 0.0f, horizontalPoint);
        Debug.Log("spawnPosition: " + spawnPosition);

        if (spawnPosition.x < 0)
            lookToTheOtherSide.x = 9.0f;
        else
            lookToTheOtherSide.x = -9.0f;
        if (spawnPosition.y < 0)
            lookToTheOtherSide.z = 6.0f;
        else
            lookToTheOtherSide.z = -6.0f;

        //Instanciamos el enemigo
        Instantiate(
            objectToSpawn,
            spawnPosition,
            Quaternion.LookRotation(lookToTheOtherSide, Vector3.up)
        );

    }

}
