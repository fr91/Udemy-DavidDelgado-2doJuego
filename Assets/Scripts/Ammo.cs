using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{

    public float damage = 100.0f;
    public float lifeTime = 2.0f;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // This function is called when the object becomes enabled and active
    private void OnEnable()
    {
        CancelInvoke();
        Invoke("Die", lifeTime);
    }

    // OnTriggerEnter is called when the Collider other enters the trigger
    private void OnTriggerEnter(Collider other)
    {
        Health health = other.gameObject.GetComponent<Health>();
        if (health == null)
            return;
        health.HealthPoints -= damage;
    }

    void Die()
    {
        gameObject.SetActive(false);
    }

}
