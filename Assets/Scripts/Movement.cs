﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    public float maxSpeed = 4.0f;
    private Transform theTransform = null;

    private void Awake()
    {
        theTransform = GetComponent<Transform>();
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //S = s0 + v0 * t
        theTransform.position += theTransform.forward * maxSpeed * Time.deltaTime;
	}
}
