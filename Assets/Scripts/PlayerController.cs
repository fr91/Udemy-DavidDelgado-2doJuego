using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody theBody = null;
    private Transform theTransform = null;

    public float maxSpeed = 4.0f;
    public float reloadDelay = 0.2f;
    public bool ableFire = true;

    public Transform[] weaponTransforms;


    private void Awake ()
    {
        theBody = gameObject.GetComponent<Rigidbody>();
        theTransform = gameObject.GetComponent<Transform>();
    }

    private void FixedUpdate ()
    {
        //VELOCIDAD
        //---------
        //Aplicamos una fuerza proporcional a como se están pulsando los ejes de movimiento

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        //Aplicamos la fuerza a nusetro jugador
        Vector3 direction = new Vector3(horizontal, 0.0f, vertical);
        theBody.AddForce(direction.normalized * maxSpeed * maxSpeed);
        //No permitimos que la velocidad sea demasiado grande
        theBody.velocity = new Vector3(Mathf.Clamp(theBody.velocity.x, -maxSpeed, maxSpeed), 0, Mathf.Clamp(theBody.velocity.z, -maxSpeed, maxSpeed));

        //ROTACION
        //-----------

        Vector3 mousePositionInScreen = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.0f);

        Vector3 mousePositionInWorld = Camera.main.ScreenToWorldPoint(mousePositionInScreen);

        mousePositionInWorld = new Vector3(mousePositionInWorld.x, 0.0f, mousePositionInWorld.z);

        Vector3 positionToLook = mousePositionInWorld - theTransform.position;
        theTransform.localRotation = Quaternion.LookRotation(positionToLook.normalized, Vector3.up);


        //Comprobar disparo de la nave
        //
        if (Input.GetMouseButtonDown(0) && ableFire)
        {
            foreach (Transform t in weaponTransforms)
            {
                AmmoManager.SpawnAmmo(t.position, t.rotation);
            }
            ableFire = false;
            Invoke("EnableFire", reloadDelay);
        }
    }


    // Use this for initialization
    void Start ()
    {

    }

    // Update is called once per frame
    void Update ()
    {

    }

    void Die ()
    {
        Destroy(gameObject);
    }

    void EnableFire ()
    {
        ableFire = true;
    }
}
