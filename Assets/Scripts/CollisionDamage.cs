﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDamage : MonoBehaviour {

    public float damagePoints = 10.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // OnTriggerStay is called once per frame for every Collider other that is touching the trigger
    private void OnTriggerStay(Collider other)
    {
        Health health = other.gameObject.GetComponent<Health>();
        if (health == null)
        {
            return;
        }
        health.HealthPoints -= damagePoints * Time.deltaTime;
    }


}
