﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public static int score = 0;
    public string scorePrefix = "Score: ";

    public Text scoreText = null;
    public Text gameOverText = null;

    public static GameController gameControllerSingleton = null;

    // Se llama a Awake al cargar la instancia del script
    private void Awake()
    {
        gameControllerSingleton = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (scoreText != null)
        {
            scoreText.text = scorePrefix + score.ToString();
        }
    }

    public static void GameOver()
    {
        if (gameControllerSingleton.gameOverText != null)
        {
            gameControllerSingleton.gameOverText.gameObject.SetActive(true);
        }
    }
}
