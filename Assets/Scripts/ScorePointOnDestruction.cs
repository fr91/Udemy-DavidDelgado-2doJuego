﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePointOnDestruction : MonoBehaviour {

    public int pointsIncrement = 200;

    // Se llama a esta función cuando se va a destruir la clase MonoBehaviour
    private void OnDestroy()
    {
        GameController.score += pointsIncrement;
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
