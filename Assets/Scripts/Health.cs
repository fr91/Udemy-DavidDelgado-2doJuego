﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public GameObject deathParticlesPrefab = null;
    private Transform theTransform = null;
    
    //serialized private fields
    [SerializeField] private float _healthPoints = 100.0f;
    public float HealthPoints {
        get
        {
            return _healthPoints;
        }
        set
        {
            _healthPoints = value;
            if (_healthPoints <= 0)
            {
                SendMessage("Die", SendMessageOptions.DontRequireReceiver); //Coste computacional alto
                if (deathParticlesPrefab != null)
                {
                    Instantiate(deathParticlesPrefab, theTransform.position, theTransform.rotation);
                    Destroy(gameObject);
                }
        }
        }
    }

    private void Awake()
    {
        theTransform = GetComponent<Transform>();
    }
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
